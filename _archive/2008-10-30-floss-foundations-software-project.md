---
layout: single
title: "FLOSS Foundations Software Project"
date: 2008-10-30
permalink: /floss-foundations-software-project
author: bkuhn
categories:
    - announcements
---

FLOSS Foundations spawned off a software project (of sorts). If you are interested in software systems to help in the management of non-profit organizations, join the [Foundations Software](http://lists.flossfoundations.org/mailman/listinfo/foundations-software) list.

---

## Comments

### Accounting for Open Source

* Author: philipdc
* Date: 2009-03-19 14:08

I am the project leader of TurboCASH Accounting. How can I be off assistance?

I looked at your Wiki - these are all complex solutions.

What I have is a straight up and down count - the - cash Accounting system. It can interface to OSCommerce and VTiger and that is all you really need. It is in fact simple to interface to most of the CRM and CMS packages. Just apply resources.

Try it out at www.turbocash.net
