---
layout: single
title: "Notes from FLOSS Foundations at OSCON 2008"
permalink: /notes-floss-foundations-oscon-2008
---

## Allison's Notes

### Update on Software for FLOSS Non-Profits:

- LedgerSMB (fork of SQL Ledger) is not ready for prime time. Goal was LedgerNPO for non-profits. Needed a code refactor, not finished yet.
- CiviCRM, Mozilla is about to deploy. Good for managing infrastructure, only very small tools for financial organization.
- TinyERP (was OpenERP), have a large set of accounting modules. Geared toward corporate use, have some modules that could be used for non-profit.
- Razor's Edge, proprietary.
- DonorWare, proprietary, built on open source WebGUI.
- Could we develop a series of Drupal modules that handle what's needed?
- RSVP tools.
- Pentabarf, software for organizing small conferences, Ruby on Rails + Postgres
- Expectation, GNOME Foundation got a free license, may be open source soon
- SugarCRM
- Concursive, CRM tools
- Outsource to Paychecks, CheckMate, PrimePay, ADP
- Mozilla could be interested in funding development of NPO software.

### Identity Commons:

- wiki.idcommons.net
- Clay Shirkey's book
- Internet Identity Workshop

### FOSS Leadership Training:

- Rockwood Feb 2009 $4,000
- A need for FOSS leadership training adapted to managing volunteers
- Also a need for technical leaders training
- Karl Fogel's book
- Google Summer of Code mentor's list has good content on
- FOSS Coach (fosscoach.wikia.com), running within OSCON (also called OSCon). Running a repeat of Poisonous People talk.
- BSD Certification and LPI has done a good
- Michael Dexter book recommendation "The E-Myth Revisited" by Michael E. Gerber
- "First Break All the Rules" by Marcus Buckingham and Curt Coffman (Help people with what they're good at, don't punish them for what they're bad at.)

### Working with Contributors:

- Clue Deficit Disorder (curable), Clue Retention Disorder (not curable), Poisonous People (dire), Truly Malicious Troll (quite rare, and easily identifiable)
- How do you work with these people?
- Reward behavior that you approve of, that benefits the community.
- Zak has created a bug-tracker to handle issues at the OSI, more productive discussion.
- When do you kick someone off the mailing list?
- Poisonous People switching projects.
- Examples of Poisonous conversations, not targeting specific people, but linking to emails. donotfeedtheanimals.com
- Transitions are natural and expected, trolls departing, developers absorbed by new jobs, people have kids.

- OSI is running training on how to triage license discussions in Trac. Sometime on Monday or Tuesday.

### Accepting Donations:

- Paypal, Click and Pledge, Google Checkout (no fees to 501(c)(3) orgs), Pay Simple, Tip Joy (http://tipjoy.com/, Ivan, Jacob Kaplan-Moss has contact)
- Linux Fund Branded Cards
- Twisted giving public credit to donors.
- Keeping in contact with donors, updates on what's happening inspires new donations.
- Blender as exemplar of acknowledging donors.

- Legal Advice for new Projects,
- Often their first interest is when they need to accept donations.
- Individual Liability is another motivation for forming foundations.
- Advocacy to end users to for why they need to look for responsible organizations.

### Trademarks:

- Allowing community use while preventing malicious use.
- Working with existing trademark holders.
- OpenJDK

### Other Topics discussed:

- How to start a non-profit organization.
- Folding foundations into existing foundations.
- Resources for new foundations.
- JCA CLA licensed under creative commons. http://www.sun.com/opensource
- Print shirts locally.
- Wiki page or yelp category of local resources could be useful.
- Educating the new wave of open source contributors
- Obama endorsed open source, new influx of government users.
- Eclipse is changing structure to accommodate companies that aren't software vendors or software developers. No commercial offering or developers on project.

- ET phone home question, whether or what data to send back to a central repository
- Freeing of proprietary software, to push maintainership back to core project

### Action Items:

- Create a new mailing list for software. software@lists.flossfoundations.org, at OSU OSL. (Done. --ANR)
- Josh Berkus, start reqs for software to support FLOSS non-profits.
- Start a page of recommended reading on floss foundations.org.
- Update the membership list.
- Put together an initial CLA list for OSI's CLA pages.

- List of software freedom related 501(c)(3)s.

- Invite Martin Michlmayr, invite the (Debian) DPL every year.
- Post to the list any developments in trademarks.
- Topical Summary of mailing list archives, starting with Dave Neary's message to new members.

- Ask OSU if we can have some pages accessible only to member logins
- Next year, bring blank name tags.

---

## Gerv's Notes

### When to create a Foundation?

* Karl: Subversion Corp - holding trademarks, taking SoC money
* Want to fold non-profit part into ASF, SFC or similar
* Don't want to change governance
* Apache may impose a governance structure...
* (Discussion of whether it does or not)
* But why are there so many? Why do people not use umbrellas?

* Allison: 3 years ago, I thought it was always good to go with an umbrella
* Now want to revise my view
* Umbrellas good to start with, when you want to focus on code
* You do get a bit more independence as your own Foundation
* Decided to split off Parrot from the Perl Foundation umbrella

Bradley: Yes, go with an umbrella first

Allison: You start with an umbrella, then branch out after you've grown

Josh: We (PostgreSQL) started as a 501c(3), realised how much work it would be, shut it down and joined SPI.

David: Be careful being a "public benefit corp" - Oregon, at least, didn't like us doing that and got nasty until we became a 501c(3).

Michael: The new form 990 is "horrible" (accountant's words) and so that might make the decision really easy when they start using it next year.

Karen: Remember there's a $25,000 limit before you have to file the nasty forms.

David: We used professional advice for this, and I sleep a lot better over it.

Allison: I drafted all the incorporation documents and contracts, but I got a lawyer to review them before we sent them off or signed them.

Karen: Remember the FSF's legal overview for free software projects.

Josh: Also can use an umbrella for a year to get the ability to raise funds to pay lawyers to do your own non-profit!

Kaliya: If you have umbrellas, how do you avoid member projects feeling that they've lost their identity?

Bradley: At least for the conservancy, people are officially "member projects". There's a contract which says that we can't interfere in technical decisions. Projects create their own identity via e.g. websites.

(Long discussion about the legality of directed donations)

Kaliya: Do you take a cut of donations?

Bradley: No, but we drop hints which most people ignore.

Lance: Projects graduate. Mozilla is a good example for us. Drupal are behind them.

Josh: Carnegie Mellon takes 30%, although they supply office space.

Don: Anyone else a c(6)?

Karl: One of my two is.

Allison: We looked carefully at 6(c) before picking c(3). The key difference is whether your sponsored seats are Board seats or something independent.

Luis: GNOME has a "write a cheque" advisory board, but a board of directors sets direction.

(Discussion about public support test for 501c(3).)

Michael: If one umbrella project gets sued, does it affect the others?

Josh: It depends on the relationship between the sub-projects and the umbrella. SPI operates like a service, and the projects are not members, so it shouldn't have any effect.

Karen: A conservancy is different. Different structures have pros and cons. There is more risk to other projects in our model, so conservancy boards must have a diligence process to look at what the liabilities are.

Dave: Anything better than Paypal for small donations? They are a leech.

Josh: We used Click and Pledge for a while, but had issues. Consider getting a merchant account if your volumes are high enough.

Bradley: Only one credit card processor has a full free software stack. Google Checkout still gives no fees to 501c(3)s, so they are the people to use now.

Jacob: Although they refuse payments from China and Russia.

Josh: Google Checkout problems: limited in forms of payment they accept. Also, can't do anything but take money. Can't, for example, add a note. (Bradley thinks you can.) But also bigger Google policy of never being able to get anyone on the phone. When you have a financial problem, you need to be able to do that. Now switching to PaySimple.
