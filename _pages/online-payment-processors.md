---
layout: single
title: "Online Payment Processors"
permalink: /online-payment-processors
---

This is an expanded version of the notes from the 2009 SCaLE meeting.

## Google Checkout

http://checkout.google.com/

Pro:

* Low Fees, even if not 501(c)(3)
* Discounted fees for 501(c)(3) organizations
* Automatic transfer of money to designated account via ACH
* E-mail support within a few days

Con:

* No last-resort phone number
* Useless reporting, data export
* API use requires SSL certificate
* Allows only one discounted account for each 501(c)(3) and will randomly suspend one of two, even if fees are paid
* 501(c)(3) discounts now require use of AdWords

## PayPal

http://paypal.com/

Pro:

* Wide user base

Con:

* High fees
* Varying US/Foreign fees
* Account blocking solution to nearly all issues such as chargebacks
* Additional e-mail addresses will periodically disappear
* Canned-answer tech support
* Broken reporting export
* Relatively high rate of fraud

## Click 'n' Pledge

http://clickandpledge.com/

* Many donation types

## Network For Good

http://www.networkforgood.org/

* Is a nonprofit
* Is hard to reach

## Pay Simple

http://www.paysimple.com/

* Low fees
* No reporting
* Has API

## tipjoy

http://tipjoy.com/

* No comments yet

## Trust Commerce

http://www.trustcommerce.com/

* Had API
* Was great
* Now too big

## authorize.net or your own credit card processing

http://authorize.net/

* May require full-time security person
* Be cautious of gray-market processors

## Notes

European IBAN transfers are nice. All other methods are flawed.

$1 or $.01 payments are usually tests for fraudulent use, often with a chargeback to another account

Many users initiate a chargeback based on the weird entry on their credit card statement, such as "SPI" instead of "PostgreSQL"

Tip: Save money and account standing by sending checks to those who request chargebacks.

Tip: Some banks will offer a "lock box" post office box to which you can send checks and they will deposit them, directing all other mail to a specified address. Cost at Wells Fargo, California: $250/month [More information](https://www.wellsfargo.com/com/treasury_mgmt/receivables/lockbox).
