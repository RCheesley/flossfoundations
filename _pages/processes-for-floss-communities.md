---
layout: single
title: "Processes for FLOSS Communities"
permalink: /node/7
author: Kaliya Hamlin
---

## Key Ideas

* Community is expanding (Kaliya references Homesteading the Noosphere)
* Facilitating communication in this broader community - especially in communications between the core team and users.
* Open Space Technology as process to help this process (http://en.wikipedia.org/wiki/Open_Space_Technology)

See also:

* http://www.kaliyasblogs.net/unconference/
* http://www.kaliyasblogs.net/Iwoman/

## Other threads of discussion

* Where is the value in standard conferences?
* People noted that early on, they followed the standard tracks in conferences and then later found that the value was in meeting people (aka "The Hallway Track"
* How can you capture the result of face to face meetings?
* Have expert meeting documentors and facilitators
* Record and then clean up afterwards
* People documenting meetings shouldn't be afraid to ask for clarification
* It is great to know what processes various groups engage in - that really helps us innovate as a group
* How can we get more of these?
* How can we balance formality vs. improvization?
* Don't be afraid to break stuff
* Don't be afraid to start a bit faster, sloppier, etc.
* How do we communicate better with people?
* How can we enable better communication between the FLOSS community and the public?
* How does this matter?
* How the hell can we sum up this discussion? Sounds like a good topic to work on tomorrow?

## Green Software

A neat alternate term by Julian
