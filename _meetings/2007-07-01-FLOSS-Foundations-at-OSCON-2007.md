---
layout: single
title: "FLOSS Foundations at OSCON 2007"
date: 2007-07-01
permalink: /node/4
categories:
    - meetings
---

The meeting is a place for the people who try to Get Stuff Done at various open source projects and non-profits around open source to congregate, share information, and pick each others' brains.

## When? Where?

July 23 and 24 in Portland, Oregon (USA). The summit was held immediately before [OSCON](http://conferences.oreillynet.com/os2007), at the [Oregon Convention Center](http://www.oregoncc.org/).

## Who?

* Allison Randal, board of directors for the Perl Foundation (organizer of the summit)
* Lance Albertson, [Open Source Lab](http://www.osuosl.org/)
* David Boswell, [Mozilla Foundation](http://mozillafoundation.org/) and [Mozdev Community Organization](http://www.mozdev.org/)
* Richard Dice, steering committee chair for the Perl Foundation
* [Justin Erenkrantz](http://www.erenkrantz.com), [The Apache Software Foundation](http://www.apache.org/)
* [Zak Greant](http://zak.greant.com/), [Mozilla Foundation](http://mozillafoundation.org/) Ombudslizard - other affiliations are listed at http://zak.greant.com/affiliations
* Scott Kveton, [OpenID](https://openid.net) Ambassador
* [Rob Lanphier](http://web.archive.org/web/20070519033058/http://wiki.secondlife.com/wiki/User:Rob_Linden), [Linden Lab](https://www.lindenlab.com)
* [Ted Leung](https://www.sauria.com/blog/), [Open Source Applications Foundation](http://www.osafoundation.org)
* [Keith Lofstrom](http://www.keithl.com), KLIC, OpenICED
* Tom Marble, [OpenJDK](http://openjdk.java.net/) Ambassador
* Gervase Markham, Mozilla Foundation
* Mike Milinkovich, exec director, Eclipse Foundation
* Bill Odom, president of [The Perl Foundation](http://perlfoundation.org/)
* Josh Berkus, PostgreSQL.org and Software in the Public Interest
* Simon Phipps, OpenJDK Board, OpenSPARC Board, Sun Open Source
* Tiki Dare, Sun's chief trademark counsel
* Donnie Berkholz, Gentoo
* [David Mandel](http://davidmandel.com), Executive Director of [Linux Fund](http://linuxfund.org) - Also the [Portland Linux/Unix Group](http://pdxlinux.org)
* [Frank Hecker](http://hecker.org/), [Mozilla Foundation](http://www.mozilla.org/foundation/)

## Agenda

Topics discussed on each day.

### Day 1: Round Table Discussion

* Introductions
* OpenICD: Open source as business retirement path. Licenses, structures for new foundations. Easing others in while founders ease out.
* Why Not Donor Listings?
* Lunch
* European Incorporation
* Elections & Governance
* Basic Fundraising Techniques
* Open Source Business models
* Open Source as a badge of honor
* Time management for the chronically over-stimulated ...

### Day 2: Round Table Discussion

* Introductions
* Open Source & Trademarks
* "Powered by", giving credit for use of a project
* Bookkeeping for NGOs
* Software for managing open source foundations: payouts, receipt, SQLLedger
* Steel cage death match where we as leaders of the FLOSS community triumph over the crap that is the US trademark system
* Open Patents
* Bookkeeping for NGOs, European Incorporation, Why Not Donor Listings?, Elections & Governance, Basic Fundraising Techniques
* Open Source & Trademarks (Tuesday)
* Copyright, trademark, patents, etc.. Governance.

### Action items

* Wiki page that links to all CLAs for open source projects, resource for new projects. (Don't start from scratch.)
* Separate mailing list for software discussions. (Call for input from mailing list.)
* List best vendors (shirt production/sales, etc.) on Wiki
* Invite someone from Debian to join mailing list
* List of software freedom related 501(c)(3)s
* List of companies who do employer matching
* Ask about approved charities for employer matching at key 10 companies
