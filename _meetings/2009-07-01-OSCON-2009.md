---
layout: single
title: "OSCON 2009"
date: 2009-07-01
permalink: /oscon-2009
categories:
    - meetings
---

The face-to-face meetings are a place for the people who try to Get Stuff Done at various open source projects and non-profits around open source to congregate, share information, and pick each others' brains. This year we will meet in July in San Jose California (USA). The meeting will be held on the Sunday and Monday before [OSCON 2009](http://en.oreilly.com/oscon2009), partly overlapping and in co-operation with the [Community Leadership Summit](http://www.communityleadershipsummit.com/) being organised by Jono Bacon.

Monday: Start at 10am in room B2.

Attendee map (in clockwise order):

* Dave Neary (GNOME)
* Sylvia Aimasso (FOSS Foundation for Africa)
* Nnenna Nwakanma (OSI, FOSS Foundation for Africa)
* Elin Waring (Joomla)
* Larry Rosen (Attorney)
* Henri Yandell (Apache)
* Michael Dexter (Linux Fund)
* Michael Tiemann (President of OSI)
* Hiram Wright (Subversion)
* Justin Erenkrantz (President, Apache)
* Sander Striker (Exec. VP Apache)
* Paul Querna (Apache VP Infrastructure)
* Allison Randal (Perl Foundation, Parrot Foundation)
* Gervase Markham (Mozilla Foundation)
* David Mandel (Linux Fund)
* Ilan Rabinovitch (SCALE, Linux Fund)
* Tiki Dare (Sun Microsystems)
* Donald Smith (Eclipse)
* David Boswell (Mozilla Foundation)
* Milton Aineruhanga (FOSSFA)
* Joe Brockmeier (OpenSUSE)
* Simon Phipps (Sun Microsystems)
* Bradley M. Kuhn (SFLC, Software Freedom Conservancy)
* Erin Williamson (SFLC Attorney)
* Alolita Sharay (OSI)

Attendees: (add yourself here)

* Dave Neary
* Jeff Sheltren
* Gerv Markham (Monday only)
* Bradley M. Kuhn
* Simon Phipps
* Donald Smith
* Allison Randal
* Justin Erenkrantz (Monday only)
* Elin Waring
* Jacob Kaplan-Moss
* Louis Suárez-Potts
* David Mandel (LinuxFund.org)
* Tiki Dare
* Bruce Perens

## Related materials

* [European Union NGO Resources](/european-union-ngo-resources)
* [Software Freedom day](https://www.softwarefreedomday.org)
* [Pledgie](http://web.archive.org/web/20090715032438/http://pledgie.com/)
* [Ledger](http://web.archive.org/web/20090712032601/http://wiki.github.com/jwiegley/ledger)
* [Open STV](https://www.opavote.com/?openstv=1)
* [STV system](https://sourceforge.net/projects/votesystem/)
* [Financial Stewardship](http://web.archive.org/web/20090605194613/http://financialsteward.org/)

Suggested books:

* [Remix by Larry Lessig](https://www.amazon.com/Remix-Making-Commerce-Thrive-Economy/dp/1594201722)
* [Conversation Capital](http://www.amazon.com/Conversational-Capital-Create-Stuff-People/dp/0137145500)

## Dave Neary's meeting notes

Notes from the meeting of the FLOSS Foundations group on Monday before
OSCON 09 in San Jose.

Start with a round-table with brief introductions, and short statement
of what people were looking for from the day.

(apologies to anyone not listed who came in late - please add people you
see missing)

* Dave Neary, GNOME Foundation
    * International affiliate organisations
    * The role of the non-profit leadership in setting tone and enforcing behavioral norms
* Nnenna, FOSSFA 
    * Fundraising
* Elin Waring, Open Source Matters
    * Role of non-profit in project ecosystem
* Larry Rosen
    * Wants to hear what we all need
* Henri Yandell, Apache Foundation
    * Trademarks
    * Apache attic (retiring dead projects)
* Michael Dexter, LinuxFund
* Michael Tiemann, OSI
* Jeff Sheltren, OSU OSL
    * Wants to hear what we need from OSL
* Justin Erenkrantz, Apache Foundation
* Milton Aineruhanga, FOSSFA
    * Getting support from other organisations for FOSSFA activities
* Sander Striker, Apache Foundation
    * Consolidation of non-profits, umbrella organisations
* Cat Allman, Google
    * Working better with communities
* Paul (Apache Foundation, I think)
    * Scaling communities
* Allison Randall, Parrot & Perl foundations
    * Maintaining energy in a project over time (bringing in the new blood
    * & renewing & revitalising the mission)
* Gervase Markham
    * To listen & help out
* David Mandel, LinuxFund
    * Fundraising
    * What can we learn from liberal arts models?
    * Incubating projects
* Deb Bryant, OSU OSL
    * Learn about other communities
* Greg Lund-Chaix, OSU OSL
* Tiki Dare, Sun Microsystems (OOo & OpenSolaris)
    * Trademarks - wants to know where is the debate
    * International consequences & variations of trademark
* David Boswell, Mozilla
    * Fundraising tools, CiviCRM
    * Tools for visualising community
* Bradley Kuhn, SFLC & Software Freedom Conservancy
* Aaron Williamson, SFLC
* David Maxwell, NetBSD
    * Workflow tools for managing board tasks
* Simon Phipps, Sun
* Silvia Aimasso, FOSSFA
    * Fundraising
* Danese Cooper, OSI
* Lance Albertson, OSU OSL
* Joe
* Ilan Rabinovich, SCALE
* Alolita Sharma, OSI

After the introductions (10:30), we split the day up, based on what people wanted from the day, into 4 major sections:

* People ~1h
* Legal ~1h (trademarks mostly)
* Money ~2.5h (getting it & using it)
* Tools ~1h

And we finished the day with a session summary, and suggestions for future sessions.

## People & culture

Dealing with projects that die: the Apache Foundation presented their
attic as a technical & social way to indicate that projects are
unmaintained. Some questions around potential IP issues - agreed that
for free software projects with no dual licensing it's not necessarily
an issue.

Allison pointed out that energy levels can tend to dissipate or go down
in a project over a longer period, especially if you don't get new
developers coming in. We talked for a while about how to enable or
encourage your project leaders to make themselves dispensable. Some
tidbits I noted:

* Leave a vacuum - sometimes it's good to leave something small but obvious undone, to create an easy way for someone else to get started
* Actively recruit partial replacements - ask people to do something small for the project which lightens your load
* Set a deadline on the effort - ask someone to do something for a year, with a clear end date. After a year, it's always possible to renew, but this allows someone to step back with no guilt.
* Lessons from companies: leaders get replaced because of the company imperative to grow or die. No explicit recruitment usually
* Be clear about what you're not going to do. Sometimes saying "this is important, but I'm not going to do it" is enough of a nudge to empower someone to carry the torch.

### How do you track developers in your projects?

* Blogging about tools and hoping people add themselves (GNOME map, calendars, user group wiki pages, etc). This is the Field of Dreams model - if you build the infrastructure & communcate about it, they will come
* Mozilla campus ambassadors program & Ubuntu LoCos were two successful outreach recruiting efforts that were identified. Model: Provide tools & infrastructure, but very few requirements for local teams, and then recruit through conference presence, friends network, etc. Mostly ad hoc, Ubuntu was map based, where they identified states & countries with no formal LoCo, and actively recruited when the opportunity arose.  Distributed rules about becoming official LoCos with well described requirements helps.
* Mozilla are working on a community CRM through a paid CiviCRM developer, Jay from Mozilla knows more
* Michael Tiemann gave a book reference here: "Conversation capital" which talks about this subject. Described 2 essential motivators for people approaching a project: impact & meaning.

### Technical enablers

Someone mentioned that having a modular architecture allows developers to add value without learning the whole project. Goes with good module developer documentation

### Volunteerism?

Simon Phipps suggested that talking about volunteers in the project is framing things wrong, and is a marker that something's wrong already.  "Volunteer" suggests to him someone who we can ask to do what we would like them to do. People join community projects because of personal motivations, that we must respect.

Nevertheless, it was remarked that the best way to have someone do a task which is not being done is to ask them to do it. Very few in our communities refuse requests for help.

One interesting suggestion I heard in the hallway: Build up psychic capital for your project by asking people to do favours for you very early on. Psychologically, you owe them, and thus they're invested in the project. Or something. (Can the person who said this clarify and give the source, please?)

Other stuff:

* Involvement is graduated. Getting a new core developer starts by getting him a little involved, then a little more, then...
* Establish a trust pipeline. Make your project scalable by having trusted lieutenants whose judgement you trust implicitly for certain types of decision. Write down these lieutenants and the things you trust them for (Mozilla superreviewers, Subversion module owners, kernel subsystems all work like this)
* Renew & revise vision regularly - to change your goals, or to plan a new path there from where you are currently.
* Apache project incubator cited as a good example of a mentor programme which teaches people the project culture as part of the induction
* Google Summer of Code credited with "forcing" projects to establish formal mentor programs.
* Write everything down in this area. Make it easy to point newcomers to documentation on how the community's social dynamic works.
* Project bans: Open Source Matters has a theoretical ban capability, administered by the community in extreme cases of bad behaviour (illegal behaviour, threats of physical violence, hate speech). Currently 3 active bans, 1 under review.
* Values to enforce to maintain harmony & nurturing environment:
    * Mutual respect
    * Consensus building
    * Respect for decisions once made

## Trademarks

Yadda yadda same ol' same ol' (This is unfair to people involved in this discussion, I think some new ground was covered, but I am so not a lawyer).

* Suggested trademark policy to base off: Fedora
* Trademark policy: "what you're letting people do without requiring a licence"
* You may not get to decide what requires a licence or not. Then again, maybe you can.
* We talked about enforcement, and what you need to do
* There was some talk about international differences in trademark law, and the cost of internationally registering a trademark
* Someone suggested using association with the mark & encouraging good behaviour. useful for valuable marks. "You can say you're using Joomla!  if you publish your changes"
* Trademark essentially serves your brand. Some organisations will try hard to get some reflected glory through association with our brands which represent community, integrity, etc. Don't sell ourselves cheap.

## Money

### Fundraising

* Need money for stuff.
* Basically two ways to get money:
    * Philanthropy ("we're dong great stuff, help us continue")
    * Value ("This thing we're organising will rock, it's well worth the registration fee")
* Ideas for ways to raise money:
    * Conference sponsorship and pricing (example: Boost has 60K in the bank of Software Freedom Conservancy through their annual conference - price ~$600
        * Nnenna reminded us that Idlelo 4 will be in Accra, Ghana from May 17 to 21
    * Self-published books (eg. FSF, Blender)
    * Swag - t-shirts, badges, etc.
    * Small donors (Friends of GNOME)
    * Corporate support (advisory board, membership fees)
    * Credit card fees (LinuxFund)
    * Donations of "stuff" (used books, cars, anything the non-profit can resell or scrap)
* Suggestion: should we band together for a major fundraising campaign?  (eg. Use Software Freedom Day to run fundraising drive where we encourage people to donate $100 to two of their favourite projects),
* CiviCRM came up again as donor management software - ref. Karl Fogel's post on the list earlier this year (reminder to self: get Donald Lobo on the list)
* For some reason, in my notes 2 book titles appear around here: "Remix" from Larry Lessig, and "Good copy, bad copy". I know Michael Tiemann mentioned them, I don't remember the context.
* Account for unpaid contributions where possible - value them with a dollar value, and record them as in-kind donations to the activities of the non-profit. This will boost your balance books, help you show public interest ti the IRS if the need arises, and make it easier to get big donations. "We have a $500,000 budget" and "We contributed $5,000,000 work to the community last year" don't have the same effect.

[Philanthropists like to contribute to causes which are having a big effect](http://web.archive.org/web/20090321091101/http://www.blueavocado.org/content/tracking-volunteer-time-boost-your-bottom-line-complete-accounting-)

### Spending money

Question: Now that we have some money, how do we spend it usefully in the community? How do we prevent money changing community dynamics?

General non-staff expenditures people mentioned: bank fees, travel funds, bounties (not very popular), internships, funded work which isn't otherwise getting done.

General staff expenditures people commonly have: administrative & support staff: system administrator, executive director, marketing & business development, community manager, conference co-ordinator

Mozilla: Brings people to conferences, buys hardware occasionally, invests in funded projects in areas like accessibility, through subcontractors, has lots of developers

Simon Phipps: Important not to spend money in your core mission, but to spend it in support functions, unless your community agrees & requests that you hire someone

LinuxFund: Asks developers to decide who will do the work for money.

Bradley Kuhn: Transparency is the key. If everyone can see the benefit coming from the expenditure, things go more smoothly.

Avoid directed funds if possible. In umbrella organisations, ensure that the things you want to spend money on are aligned with their mission.  With directed funds, bill the administrative overhead to managing the fund to the directed funds (typical overhead between 5 and 10 percent normal).

Example of "failed" funding project: Debian release manager work

## Tools

### Conference management

* SCALE Reg - SCALE's free software registration app
* Utah Open Source Conference has one too
* Pentabarf
* Conference module for Drupal (pain in the ass for multi-year conferences)
* Don't use WebERP. Someone recommended against it
* OS Bridge used Open Conferenceware
* OSCON = Expectnation, Edd still gives free licences for non-profit run
* conferences
* Badge scanners - sponsors like 'em. Cheap systems possible (ref. SCALE)

Notes say "E-Learning Africa: "ICWE" management, but I don't remember what that's about

### Foundation finances management

* GnuCash still OK
* Bradley Kuhn uses Ledger w/ ASCII text files
* CiviCRM for donor management
* Pledgie & Facebook Causes identified for campaigns
* See what Wikimedia used for their campaign
* Payment processing:
    * Google Checkout - still expensive, still possible to get cheap for non-profits if you sign up for Google Grants, more hoops to jump through, takes a year, pinging Leslie helps
    * Paypal
    * Donor's Choice
    * Kiva
    * Trust Commerce
    * Advice: make payment fees transparent. If someone clicks on the "donate $10" button on your site, given them a choice of $10 for them or $10 for you, and explain what the charges are in each case.
    * No point getting set up as a credit card merchant, lots of pain.

### Community tracking & management

* Mozilla working on a "people" CRM - people add their own details, you convince them to add the details & let community peer pressure keep it current.
* Based on CiviCRM
* Mel Chua has similar tool for Fedora
* djangopeople.net another example
* Top-down CRM also an option, give accounts generously
* Email & calendar integration is important for CRM to be successful in the community
* Voting software: Everyone using home-rolled databases & web apps/shell apps w/ ssh auth, email based voting
* OpenSTV can count STV elections (GNOME, Maemo)
* See Derek Cockburn's Internet Governance Project
* See Proxyvote.com also
* Board workflow software:
    * IssueTracker
    * Bugzilla
    * Wiki & email
* Open board meetings with treasurer report (how much we've spent, where we are wrt budget) - good practice.

## Suggestions for future co-operation

* Common fundraising drive
* Co-ordinate for pushing press releases - perhaps pool resources for press relations representative
* Pool experience on conference organising to drop hotel room prices, lower event costs, and generally make a bigger market that people have an incentive to be nice to.
* Work on an agenda before the meeting
* Another suggestion I got after the conference: when working on the agenda for next meeting, start by publishing past meeting notes, to avoid revisiting old discussions without producing anything new 
* Maybe piggyback on the Community Leadership Summit again next year
